<?php /* Template Name: Press */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>
		<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?> <!-- Press header -->
		<?php endwhile; ?>

			<!-- Featured post block -->
			<?php
				$args = array(
					'posts_per_page'   => 1,
					'offset'           => 0,
					'orderby'          => 'post_date',
					'order'            => 'DESC',
					'post_type'        => 'press-release',
					'post_status'      => 'publish'
				);

				$featured_posts = query_posts( $args ); ?>
				<div class="featured-press">
					<?php foreach ( $featured_posts as $post ) : setup_postdata( $post ); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<h2><?php the_date('m . d . Y') ;?></h2>
							<h1><?php the_title(); ?></h1>
							<a id="read-more" href="<?php the_permalink(); ?>">Read More </a>
						</article>
					<?php endforeach; ?>
				</div>
				<!-- /Featured post block -->

				<!-- Post list block -->
				<div class="press-list">
					<?php
				$args_left = array(
					'posts_per_page'   => 5,
					'offset'           => 1,
					'orderby'          => 'post_date',
					'order'            => 'DESC',
					'post_type'        => 'press-release',
					'post_status'      => 'publish'
				);
				$all_posts_left = query_posts( $args_left );

				$args_right = array(
					'posts_per_page'   => 5,
					'offset'           => 6,
					'orderby'          => 'post_date',
					'order'            => 'DESC',
					'post_type'        => 'press-release',
					'post_status'      => 'publish'
				);
				$all_posts_right = query_posts( $args_right ); ?>

				<div class="list-container">
					<ul class="all-posts-left">
						<?php foreach ( $all_posts_left as $post ) : setup_postdata( $post ); ?>
							<li>
									<h2><?php the_date('m.d.Y') ;?></h2>
									<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
							</li>
						<?php endforeach; ?>
					</ul>
					<ul class="all-posts-right">
						<?php foreach ( $all_posts_right as $post ) : setup_postdata( $post ); ?>
							<li>
									<h2><?php the_date('m.d.Y') ;?></h2>
									<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
				<!-- /Post list block -->

				<?php wp_reset_postdata();?>
		</section>
	</main>

<?php get_footer(); ?>
