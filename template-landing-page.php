<?php /* Template Name: Landing Page */ ?>
<html>
	<head>
		<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/landing-style.css" />
	</head>
	<body>
                <div class="header">
					<div class="tanner-logo"></div>
					<h1>Network Security Testing Services</h1>
					<h2 class="subhead">From a Utah Based Company</h2>
                                        <hr/>
					<p class="description">Obtain an accurate understanding of your security and risk
						posture, while ensuring compliance with industry regulators
						and information security best practices.</p>
					<p class="call-today"><a href="tel:8018891383">Call today (801) 889-1383</a></p>
			</div>
			<div class="customers"></div>
		<div class="container">
			
			<div class="main-content">
				<div class="wide-column">
					<div class="blurb1">
						<h2 class="subhead-two">Why <span style="font-style:italic;">local</span> companies trust Tanner:</h2>
						<p><em>Insightful</em> - Comprehensive and accurate testing and reporting mythologies</p>
						<p><em>Easy</em> - Minimal time commitment from your technical staff</p>
						<p><em>Dedicated</em> - A singular point of contact will oversee the whole process</p>
						<p><em>Trusted</em> - Locally owned and operated for 75 years</p>
					</div>
					<div class="blurb2">
						<h2>Key Benefits:</h2>
						<p>+ Obtain an accurate understanding of your security and risk posture</p>
						<p>+ Comprehensive reporting, relevant to your organization and stakeholders</p>
						<p>+ Comply with industry and information security best practice</p>
					</div>
					<h2>Utah's Best</h2>
					<div class="utahs-best"><img src="../wp-content/themes/tanner2015/img/utahs-best.png"></div>
					<h2>Stay Ahead of Evolving Risk</h2>
					<p>IT departments are working hard to protect their network from potential threats.  This can be a daunting task, because of the rapidly evolving techniques and persistence of cyber criminals. To make matters worse, a barrage of new hardware and software is consistently hitting the market, each one promising to protect against and solve every security concern.  Unfortunately, these products often do very little to actually protect the network, and the biggest impact they have on a business is a major expense and provide management with a false sense of security.</p>
					<p>Tanner has a different approach. A third-party IT Security Assessment will provide management with the critical insights necessary for building an effective security program and to protect against technical risk. We understand that each company has a unique set of security needs, and we believe that those needs must be taken into consideration when determining what safeguards should be implemented.  Determining factors can include things such as:</p>
					<ul class="small-text">
						<li>Business operations and procedures</li>
						<li>Compliance regulations</li>
						<li>Risk tolerance</li>
					</ul>
					<div class="experts">
						<h3>Work with Our Experts</h3>
						<p>John spent the last five years of his career with a leading IT company, Wazi Technical Solutions, where he developed a passion for business management and growth with a specific interest in Network Security. He is a frequent speaker on IT controls and network security topics at meetings for professional organizations and business groups. John received his Master�s in Business Administration, summa cum laude, from Westminster College in 2011.</p>
					</div>
					<div class="blurb3">
						<p>Obtain an accurate understanding of your security and risk posture, while ensuring compliance with industry regulators and information security best practices.</p>
						<p class="call-today"><a href="tel:8018891383">Call today (801) 889-1383</a></p>
					</div>
				</div>
				<div class="narrow-column">
					<div class="sidebar-box">
						<h4>Engage the security experts</h4>
						<p>Learn how Tanner�s security services can help you accomplish your specific business goals.</p>
<div class="sidebar-form"><?php echo do_shortcode( '[contact-form-7 id="2139" title="Network Security Testing Services - Contact form"]' ); ?></div>
					</div>
					<div class="sidebar-box">
						<h4>Typical services involved in a security review include:</h4>

						<ul class="small-text">
							<li>External Network Assessment</li>
							<li>Internal Network Assessment</li>
							<li>Network Configuration Review</li>
							<li>Active Directory Reviews</li>
							<li>Wireless Security Penetration Testing</li>
							<li>Firewall Security Reviews</li>
							<li>Application Assessments</li>
							<li>Social Engineering Testing</li>
						</ul>
					</div>
					<blockquote class="sidebar-blurb">
						<p>"We love working with the Information Security team at Tanner. They customized their service offerings to fit out needs, and put together a team of well qualified individuals to work with us. Their team has exceeded my expectations and I am happy to refer Tanner LLC to others!"</p>
						<cite class="citation">
							Steve Scott
							<br />Riverton City
						</cite>
					</blockquote>
					<div class="download">
                                                <?php if( get_field('brochure') ): ?>						     
                                                         <a href="<?php the_field('brochure'); ?>">Download Brochure</a>
                                                <?php endif; ?>
<?php if( get_field('white_paper') ): ?>
						<a href="<?php the_field('white_paper'); ?>">Download White Paper</a>
<?php endif; ?>
					</div>
				</div>
			</div>
		</diV>
	</body>
</html>