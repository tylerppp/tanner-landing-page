
<!-- article -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="post-header">
    <!-- post title & details -->
    <div class="post-details">
      <h1>
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
      </h1>
      <?php the_tags( '<ul class="post-tags"><li>', '</li><li>', '</li></ul>' ); ?>
      <!-- place content here -->
      <p><?php the_excerpt(); ?></p>

    </div>
    <!-- /post title & details -->

    <!-- post author -->
    <?php if(get_post_type() != "press-release"): ?>
      <?php
        $avatar = get_avatar(get_the_author_meta('ID'));
        $name = get_the_author();
        $title = get_the_author_meta('description');
        $email = get_the_author_meta('user_email'); ?>
      <?php if($name != "admin"): ?>
        <div class="post-author">
          <?php echo $avatar; ?>
          <span id="author"><?php _e( 'by', 'tanner2015' ); ?> <?php the_author_posts_link(); ?></span>
          <span id="author-title"><?php echo $title; ?></span>
          <a class="button_teal author-contact" href="mailto:<?php echo $email; ?>">Contact Me</a>
        </div>
      <?php endif; ?>
    <?php endif; ?>
    <!-- /post author -->
  </div>

  <div class="post-content">&nbsp;</div>

  <?php edit_post_link();?>

</article>
<!-- /article -->
