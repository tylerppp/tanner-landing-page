(function ($, root, undefined) {

	$(function () {

		'use strict';

		//mobile menu
		$('.hamburger').click(function(){
			$('.mobile_menu #menu-main-menu-1').slideToggle();
		});

		$('.industry-img:first-child').addClass('active');
		$('.industry-description:first-child').addClass('active');

		$('.industry-img').click(function(event){
			event.preventDefault();
			event.stopPropagation();

			var $e = $(this),
					$old_desc = $('.industry-description.active'),
					$id = $e.attr('id'),
					$new_desc = $('.industry-description#' + $id),
					$did = $old_desc.attr('id'),
					$other = $e.siblings('.active');

			if ($id != $did) {
				$('.industry-description').hide("slide");
				$new_desc.show("slide");
			}

			if (!$e.hasClass('active')) {
        $other.each(function(index, self) {
          var $this = $(this);
          $this.removeClass('active');
          $old_desc.removeClass('active');
        });
        $e.addClass('active');
        $new_desc.addClass('active');
    	}
		});

	});

})(jQuery, this);
