<?php /* Template Name: Sub Service */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>



		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- If post thumbnail, show as page title background -->
			<?php if ( has_post_thumbnail() ) { $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), false, '' ); ?>
				<div class="page-title" style="background-color:#009e92;background-image: url(<?php echo $src[0]; ?>);">
					<h2 class="text-center text-white"><?php the_title(); ?></h2>
				</div>
			<?php } ?>

			<div class="grid__container">
				<!-- article -->
				<article id="post-<?php the_ID(); ?>" <?php post_class('column-left'); ?>>

					<!-- If NO post thumbnail, show simple page title -->
					<?php if ( !has_post_thumbnail() ) { ?>
						<div class="grid">
							<div class="grid__item page-header desk--three-sixths push--desk--one-sixth">
								<div class="">
									<h1><?php the_title(); ?></h1>
								</div>
							</div>
						</div>
					<?php } ?>

					<div class="grid">
						<div class="page-content grid__item desk--six-twelfths push--desk--one-twelfth">
							<?php the_content(); ?>
						</div>
						<div class="sidebar grid__item desk--four-twelfths push--desk--one-twelfth">
							<div class="promo">
								<p class="text-center"><img src="<?php echo get_template_directory_uri(); ?>/img/promo_header.jpg" alt="Connect with us" /></p>
								<p class="text-secondary"><strong>We would love to connect with you and offer our complimentary insights.</strong>
									 Visit our contact page to start a conversation:</p>
								<p class="text-center"><a class="button small" href="/contact">Contact Us</a></p>
							</div>
						</div>
					</div>



					<?php if(get_field('callout_1') or get_field('callout_2') or get_field('callout_3')) { ?>
						<div class="grid">
							<div class="grid__item one-third">
								<div class="block--teal">
									<p><?php the_field('callout_1'); ?></p>
								</div>
							</div>
							<div class="grid__item one-third">
								<div class="block--teal">
									<p><?php the_field('callout_2'); ?></p>
								</div>
							</div>
							<div class="grid__item one-third">
								<div class="block--teal">
									<p><?php the_field('callout_3'); ?></p>
								</div>
							</div>
						</div>
					<?php } ?>

					<?php	if(get_field('video')) { ?>
				</div>
				<div class="grid__container background--lighttan">
					<div class="grid">
						<div class="page-content grid__item desk--ten-twelfths push--desk--one-twelfth pvm">
							<div class="video-wrapper">
								<?php the_field('video'); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="grid__container">
					<?php } ?>


					<div class="grid">
						<div class="page-content grid__item desk--six-twelfths push--desk--one-twelfth">
							<h2><?php the_field('secondary_section_header'); ?></h2>
							<?php the_field('secondary_section_content'); ?>
						</div>
					</div>


					<br class="clear">

					<?php edit_post_link(); ?>

				</article>
				<!-- /article -->
			</div>


			<?php
				// If ACF quote is filled out, show blockquote banner
				if(get_field('quote')){
			?>

			<div class="grid__container blockquote-bar">

				<div class="grid">
					<div class="grid__item desk--two-thirds push--desk--one-sixth">
						<blockquote>
							<?php the_field('quote'); ?>
						</blockquote>
					</div>
				</div>

			</div>

			<?php } ?>

		<?php endwhile; ?>
		<?php endif; ?>

		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
