(function ($, root, undefined) {

	var tanner = {};

	tanner.stickyHeader = function(){
		var waypoints = $('body').waypoint({
	    handler: function(direction) {
	      $('.js-sticky-header').toggleClass('stuck');
	    },
	    offset: -150
	  });
	};

	$(function () {

		'use strict';

		//mobile menu
		$('.hamburger').click(function(){
			$('.mobile_menu #menu-main-menu-1').slideToggle();
		});

		$('.industry-img:first-child').addClass('active');
		$('.industry-description:first-child').addClass('active');

		var elems = document.getElementsByClassName( "industry-img" ),
				arr = jQuery.makeArray( elems );

		$('.industry-img').click(function(event){
			event.preventDefault();
			event.stopPropagation();

			var $e = $(this),
					$old_desc = $('.industry-description.active'),
					$id = $e.attr('id'),
					$new_desc = $('.industry-description#' + $id),
					$did = $old_desc.attr('id'),
					$other = $e.siblings('.active');
					console.log($old_desc.index());

			if ($id != $did) {
				if($(this).index() < $old_desc.index()) {
					$('.industry-description').hide('slide', {direction: 'left'});
					$new_desc.delay(400).show('slide', {direction: 'right'});
				} else {
					$('.industry-description').hide('slide', {direction: 'right'});
					$new_desc.delay(400).show('slide', {direction: 'left'});
				}
			}

			if (!$e.hasClass('active')) {
        $other.each(function(index, self) {
          var $this = $(this);
          $this.removeClass('active');
          $old_desc.removeClass('active');
        });
        $e.addClass('active');
        $new_desc.addClass('active');
    	}
		});

		$('#nomination-type').change(function(){
			var value = $(this).val();
			if(value == "Self Nomination"){
				$('.wpcf7 .left:nth-child(4) input').css('opacity', .3).prop("disabled", true);
			}else{
				$('.wpcf7 .left:nth-child(4) input').css('opacity', 1).prop("disabled", false);
			}
		});

		// Init sticky header
		tanner.stickyHeader();

	});

})(jQuery, this);
