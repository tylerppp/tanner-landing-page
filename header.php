<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ''; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' />
		<!-- <meta name="description" content="<?php bloginfo('description'); ?>">-->

		<?php wp_head(); ?>

		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<script>
		    // conditionizr.com
		    // configure environment tests
		    conditionizr.config({
		        assets: '<?php echo get_template_directory_uri(); ?>',
		        tests: {}
		    });
	    </script>

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="wrapper">

			<!-- header -->
			<?php if(the_title('','',false) != "Home"): ?>
				<header class="header js-sticky-header clear" role="banner">
					<div class="grid__container">
						<div class="grid">

							<!-- logo -->
							<div class="grid__item palm--one-third lap--one-third desk--one-sixth">
								<div class="logo logo-dark">
									<a href="<?php echo home_url(); ?>">Tanner Co</a>
								</div>
							</div>
							<!-- /logo -->

							<!-- nav -->
							<div class="grid__item palm--two-thirds lap--two-thirds desk--five-sixths">
								<nav class="nav" role="navigation">
									<?php wp_nav_menu(array('theme_location' => 'primary')); ?>
								</nav>
								<div class="mobile_menu mobile_menu_dark">
									<span class="hamburger"><i class="fa fa-bars"></i> | Menu</span>
									<?php wp_nav_menu(array('theme_location' => 'primary')); ?>
								</div>
							</div>
							<!-- /nav -->

						</div>
					</div>
				</header>
			<?php else: ?>
				<header class="header js-sticky-header header-home clear" role="banner">
					<div class="grid__container">
						<div class="grid">

							<!-- logo -->
							<div class="grid__item palm--one-third lap--one-third desk--one-sixth">
								<div class="logo">
									<a href="<?php echo home_url(); ?>">Tanner Co</a>
								</div>
							</div>
							<!-- /logo -->

							<!-- nav -->
							<div class="grid__item palm--two-thirds lap--two-thirds desk--five-sixths">
								<nav class="nav nav-white" role="navigation">
									<?php wp_nav_menu(array('theme_location' => 'primary')); ?>
								</nav>
								<div class="mobile_menu mobile_menu_light">
									<span class="hamburger"><i class="fa fa-bars"></i> | Menu</span>
									<?php wp_nav_menu(array('theme_location' => 'primary')); ?>
								</div>
							</div>
							<!-- /nav -->

						</div>
					</div>
				</header>
			<?php endif; ?>
			<!-- /header -->
