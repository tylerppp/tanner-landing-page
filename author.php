<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>

			<div class="page-title" style="background-color:#009e92;">
				<h1 class="text-center text-white"><?php _e( 'Author Archives for ', 'tanner2015' ); echo get_the_author(); ?> <span class="img-round"><?php echo get_avatar(get_the_author_meta('ID')); ?></span></h1>
			</div>



				<?php if (have_posts()): the_post(); ?>

					<?php rewind_posts(); while (have_posts()) : the_post(); ?>

						<!-- article -->
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

							<div class="post-header">
						    <!-- post title & details -->
						    <div class="post-details">
						      <h1>
						        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
						      </h1>
						      <?php the_tags( '<ul class="post-tags"><li>', '</li><li>', '</li></ul>' ); ?>
						      <!-- place content here -->
						      <p><?php the_excerpt(); ?></p>

						    </div>

							</div>

						  <div class="post-content">&nbsp;</div>

							<?php edit_post_link(); ?>

						</article>
						<!-- /article -->

					<?php endwhile; ?>

				<?php else: ?>

					<!-- article -->
					<article>
						<h2><?php _e( 'Sorry, nothing to display.', 'tanner2015' ); ?></h2>
					</article>
					<!-- /article -->

				<?php endif; ?>

				<?php get_template_part('pagination'); ?>


		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
