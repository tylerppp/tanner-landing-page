<?php /* Template Name: Insights */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>
		<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?> <!-- Insights header -->
		<?php endwhile; ?>

			<?php
				$paged = (get_query_var( 'paged' )) ? get_query_var('paged' ) : 1;
				$args = array(
					'posts_per_page'   => 10,
					'offset'           => 0,
					'orderby'          => 'post_date',
					'order'            => 'DESC',
					'post_type'        => 'post',
					'post_status'      => 'publish',
					'paged'						 => $paged
				);

				$posts = query_posts( $args );

				foreach ( $posts as $post ) : setup_postdata( $post ); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="post-header">
							<!-- post title & details -->
							<div class="post-details">
								<h1>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
								</h1>
								<?php the_tags( '<ul class="post-tags"><li>', '</li><li>', '</li></ul>' ); ?>
								<!-- place content here -->
								<p><?php the_excerpt(); ?></p>

							</div>
							<!-- /post title & details -->

							<!-- post author -->
							<?php if(get_post_type() != "press-release"): ?>
								<?php
									$avatar = get_avatar(get_the_author_meta('ID'));
									$name = get_the_author();
									$title = get_the_author_meta('description');
									$email = get_the_author_meta('user_email'); ?>
								<?php if($name != "admin"): ?>
									<div class="post-author">
										<?php echo $avatar; ?>
										<span id="author"><?php _e( 'by', 'tanner2015' ); ?> <?php the_author_posts_link(); ?></span>
										<span id="author-title"><?php echo $title; ?></span>
										<a class="button_teal author-contact" href="mailto:<?php echo $email; ?>">Contact Me</a>
									</div>
								<?php endif; ?>
							<?php endif; ?>
							<!-- /post author -->
						</div>

						<div class="post-content">&nbsp;</div>

						<?php edit_post_link();?>
					</article>
				<?php endforeach; ?>

				<?php if(!$posts): ?>
					<article>
						<h2><?php _e( 'Sorry, nothing to display.', 'tanner2015' ); ?></h2>
					</article>
				<?php endif; ?>

				<?php wp_reset_postdata();?>


		</section>
	</main>

<?php get_footer(); ?>
