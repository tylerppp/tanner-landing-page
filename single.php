<?php get_header(); ?>

	<main role="main">
	<!-- section -->
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="post-header">
			<!-- post title & details -->
			<div class="post-details">
				<span class="date"><?php the_date('m.d.Y') ;?></span>
				<h1>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</h1>
				<?php the_tags( '<ul class="post-tags"><li>', '</li><li>', '</li></ul>' ); ?>
			</div>
			<!-- /post title & details -->

			<!-- post author -->
			<?php if(get_post_type() != "press-release"): ?>
				<?php
					$avatar = get_avatar(get_the_author_meta('ID'));
					$name = get_the_author();
					$title = get_the_author_meta('description');
					$email = get_the_author_meta('user_email'); ?>
				<?php if($name != "admin"): ?>
					<div class="post-author">
						<?php echo $avatar; ?>
						<span id="author"><?php _e( 'by', 'tanner2015' ); ?> <?php the_author_posts_link(); ?></span>
						<span id="author-title"><?php echo $title; ?></span>
						<a class="button_teal author-contact" href="mailto:<?php echo $email; ?>">Contact Me</a>
					</div>
				<?php endif; ?>
			<?php endif; ?>
			<!-- /post author -->
		</div>

			<div class="post-content blog-content">


				<?php /*
					if ( has_post_thumbnail() ) {
						the_post_thumbnail();
						echo get_post(get_post_thumbnail_id())->post_excerpt;
					}
				*/ ?>


				<?php if( get_field('blog_image') ): ?>
					<img src="<?php the_field('blog_image'); ?>" alt="Post Image" class="blog-image" />
					<p><small><?php the_field('image_caption'); ?></small></p>
				<?php endif; ?>

				<?php the_content();?>

				<?php
						if(has_tag( 'podcast' )) {
				?>
					<p style="padding-top: 1rem;"><a class="button" href="https://itunes.apple.com/us/podcast/tanners-influence-podcast/id1148713203" style="text-decoration:none;">Subscribe on iTunes</a></p>
				<?php
						}
				?>
			</div>

			<?php edit_post_link();?>

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'tanner2015' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>

	</section>
	<!-- /section -->
	</main>

<?php get_footer(); ?>
