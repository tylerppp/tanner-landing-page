<?php /* Template Name: Left Content */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>



		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<?php if ( has_post_thumbnail() ) {
					$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), false, '' );
			?>
			<div class="page-title" style="background-color:#009e92;background-image: url(<?php echo $src[0]; ?>);">

					<h2 class="text-center text-white"><?php the_title(); ?></h2>

			</div>

			<?php } ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class('column-left'); ?>>

				<?php if ( !has_post_thumbnail() ) { ?>
					<div class="page-header">
						<h1><?php the_title(); ?></h1>
					</div>
				<?php } ?>

				<div class="page-content">
					<?php the_content(); ?>
				</div>

				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'tanner2015' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
