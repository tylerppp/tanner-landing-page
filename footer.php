			<footer class="footer" role="contentinfo">

				<div class="grid__container">
				<div class="grid">
					<div class="grid__item desk--one-third lap--one-third footer-nav">
						<?php wp_nav_menu(array('theme_location' => 'footer')); ?>
						<p class="social-media">
							<a class="social-media__link" href="https://www.facebook.com/tannercompany"><img src="<?php echo get_template_directory_uri(); ?>/img/Facebook.png"></a>
							<a class="social-media__link" href="https://www.linkedin.com/company/tanner-lc"><img src="<?php echo get_template_directory_uri(); ?>/img/LinkedIn.png"></a>
							<a class="social-media__link" href="https://twitter.com/tannercompany"><img src="<?php echo get_template_directory_uri(); ?>/img/Twitter.png"></a>
						</p>
					</div><!-- .footer-left -->

					<?php $uri = $_SERVER['REQUEST_URI']; ?>
					<?php if ($uri !== '/contact/'): ?>
						<div class="grid__item desk--one-third lap--one-third">
							<div class="footer-contact">
								<p>
									Let's talk.<br>
									<a class="vc_btn button_teal vc_btn_md vc_btn-md vc_btn_square" href="/contact" title="Contact Tanner">	Contact Tanner</a><br>
									or call <a href="tel:801-532-7444">801-532-7444</a> today
								</p>
							</div><!-- .footer-contact -->
						</div><!-- .footer-middle -->
					<?php else: ?>
					<div style="visibility:hidden" class="grid__item desk--one-third lap--one-third">
						<div class="footer-contact">
							<p>Let's talk.<br>
							<a class="vc_btn button_teal vc_btn_md vc_btn-md vc_btn_square" href="/contact" title="Contact Tanner">Contact Tanner</a><br>
							or call <a href="tel:801-532-7444">801-532-7444</a></p>
						</div><!-- .footer-contact -->
					</div><!-- .footer-middle -->
				<?php endif; ?>

				<div class="grid__item desk--one-third lap--one-third footer-address">
					<img src="/wp-content/uploads/2015/02/logo_dark.png" />
					<div itemscope itemtype="http://schema.org/LocalBusiness">

					<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
					<span itemprop="streetAddress"><br>36 S. State St., Suite 600</span><br>
					<span itemprop="addressLocality"> Salt Lake City</span>, <span itemprop="addressRegion">UT</span><span itemprop="postalCode"> 84111</span><br>
					Phone: <span itemprop="telephone">(801) 532 7444</span><br>
					<a href="https://www.google.com/maps/place/Tanner+LLC/@40.7682897,-111.8883969,17z/data=!3m1!4b1!4m2!3m1!1s0x8752f50e14b06675:0xa0b5d7076425dc4a" itemprop="maps">Maps & Directions</a><a href="http://tannerco.com/sitemap"> <br>Sitemap</a><br>
					</div><br>

					<div id="copyright">
						<p><?php echo date('Y'); ?> Tanner Co<br>
						All Rights Reserved.</p>
					</div></div>
				</div><!-- .footer-right -->

			</div>
			</footer>
		</div><!-- /wrapper -->

		<!-- analytics -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-62517904-1', 'auto');
		  ga('send', 'pageview');

		</script>

		<!-- Start of Async HubSpot Analytics Code -->
		<script type="text/javascript">
		(function(d,s,i,r) {
		if (d.getElementById(i)){return;}
		var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
		n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/498858.js';
		e.parentNode.insertBefore(n, e);
		})(document,"script","hs-analytics",300000);
		</script>
		<!-- End of Async HubSpot Analytics Code -->

		<?php wp_footer(); ?> 

	</body>
</html>
