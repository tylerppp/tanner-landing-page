<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>

			<div class="page-title" style="background-color:#009e92;">
				<h1 class="text-center text-white"><?php the_archive_title (); ?></h1>
			</div>

			<?php while ( have_posts() ) : the_post(); // standard WordPress loop. ?>
				<?php get_template_part( 'content-excerpt', $post_type); ?>
			<?php endwhile; // end of the loop. ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
